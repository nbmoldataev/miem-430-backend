import io
import cv2
import json
import base64

import numpy as np
import tensorflow as tf
from imutils.object_detection import non_max_suppression

from keras.backend import manual_variable_initialization

from flask import Flask
from flask import request
from flask import jsonify
from tensorflow.python.framework.errors_impl import InvalidArgumentError

manual_variable_initialization(True)
app = Flask(__name__)

model = tf.keras.models.load_model('src/ml/model.h5')
PATH_TO_EAST = 'src/ml/frozen_east_text_detection.pb'

classes = ['antqua',
           'arial',
           'bahnschrift',
           'book antiqua',
           'bookman old style',
           'calibri',
           'cambria',
           'candara',
           'century',
           'century schoolbook',
           'comic sans',
           'consolas',
           'constantia',
           'corbel',
           'courier new',
           'gabriola',
           'garamond',
           'georgia',
           'gothic',
           'haettenschweiler',
           'impact',
           'lucida console',
           'lucida sans unicode',
           'microsoft sans serif',
           'mistral',
           'monotype corsiva',
           'ms rederence sans serif',
           'palatino linotype',
           'segoe print',
           'segoe script',
           'segoe ui',
           'sitka',
           'sylfaen',
           'tahoma',
           'times new roman',
           'trebuchet ms',
           'verdana']


@app.route('/process', methods=['POST'])
def process_image():
    image = request.files.get('image', '')

    in_memory_file = io.BytesIO()
    image.save(in_memory_file)
    data = np.fromstring(in_memory_file.getvalue(), dtype=np.uint8)
    color_image_flag = 1

    img = cv2.imdecode(data, color_image_flag)

    coords = find_text(img)

    _input = tf.convert_to_tensor(img)
    new_tensor = tf.cast(_input, tf.float32) / 255.0
    tensor = tf.expand_dims(new_tensor, axis=0)

    fonts = []
    for (startX, startY, endX, endY) in coords:
        try:
            y = model(tensor[:, startX:endX, startY:endY, :])
        except InvalidArgumentError:
            continue
        indices = (tf.math.top_k(y, k=3)).indices.numpy().tolist()[0]

        f = [classes[i] for i in indices]
        pr = [y[0, i].numpy() for i in indices]
        for i in range(len(indices)):
            fonts.append({'label': f[i], 'confidence':  json.dumps(pr[i].item())})

        cv2.rectangle(img, (startX, startY), (endX, endY), (0, 255, 0), 2)
        cv2.putText(img, ','.join([classes[i] for i in indices]), (startX, endY+20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 0), 2)

    index = 0
    try:
        with open('indexing.txt', 'r') as f:
            index = int(f.read())
    except:
        pass

    _, buffer_img = cv2.imencode('.jpg', img)
    data = base64.b64encode(buffer_img).decode('utf-8')
    index += 1
    with open('indexing.txt', 'w') as f:
        f.write(str(index))
    return jsonify({'index': index, 'data': fonts, 'image': data})


def find_text(image):
    orig = image.copy()
    (H, W) = image.shape[:2]

    (newW, newH) = (320, 320)
    rW = W / float(newW)
    rH = H / float(newH)

    image = cv2.resize(image, (newW, newH))
    (H, W) = image.shape[:2]

    layerNames = [
        "feature_fusion/Conv_7/Sigmoid",
        "feature_fusion/concat_3"]

    net = cv2.dnn.readNet(PATH_TO_EAST)

    blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
                                 (123.68, 116.78, 103.94), swapRB=True, crop=False)
    net.setInput(blob)
    (scores, geometry) = net.forward(layerNames)

    (numRows, numCols) = scores.shape[2:4]
    rects = []
    confidences = []

    for y in range(0, numRows):
        scoresData = scores[0, 0, y]
        xData0 = geometry[0, 0, y]
        xData1 = geometry[0, 1, y]
        xData2 = geometry[0, 2, y]
        xData3 = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        for x in range(0, numCols):

            if scoresData[x] < 0.5:
                continue

            (offsetX, offsetY) = (x * 4.0, y * 4.0)

            angle = anglesData[x]
            cos = np.cos(angle)
            sin = np.sin(angle)

            h = xData0[x] + xData2[x]
            w = xData1[x] + xData3[x]

            endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
            endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
            startX = int(endX - w)
            startY = int(endY - h)

            rects.append((startX, startY, endX, endY))
            confidences.append(scoresData[x])

    boxes = non_max_suppression(np.array(rects), probs=confidences)

    coords = []
    for (startX, startY, endX, endY) in boxes:
        startX = int(startX * rW)
        startY = int(startY * rH)
        endX = int(endX * rW)
        endY = int(endY * rH)
        coords.append((startX, startY, endX, endY))
        cv2.rectangle(orig, (startX, startY), (endX, endY), (0, 255, 0), 2)

    return coords


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=10000)

a ={
'1 Blackmoor LET	': 'https://ofont.ru/view/51',
'28 Days Later Cyr	': 'https://ofont.ru/view/4936',
'A Font with Serifs	': 'https://ofont.ru/view/2915',
'a_BentTitulNr	': 'https://ofont.ru/view/413',
'a_BighausTitulBrkHll	': 'https://ofont.ru/view/1063',
'a_BighausTitulOtlDr	': 'https://ofont.ru/view/590',
'a_BodoniNovaNr	': 'https://ofont.ru/view/1016',
'a_Bragga	': 'https://ofont.ru/view/89',
'a_BraggaTitul	': 'https://ofont.ru/view/1637',
'a_BraggaTitulGr	': 'https://ofont.ru/view/1253',
'a_Bremen	': 'https://ofont.ru/view/712',
'a_CampusStrip	': 'https://ofont.ru/view/543',
'a_CityNovaTitulStars	': 'https://ofont.ru/view/20',
'a_CityNovaTtlSpUpLt	': 'https://ofont.ru/view/1124',
'a_ConceptoTitulNrCmGr	': 'https://ofont.ru/view/63',
'a_ConceptoTtlCmBr	': 'https://ofont.ru/view/161',
'a_DomIno	': 'https://ofont.ru/view/599',
'a_DomInoRevObl	': 'https://ofont.ru/view/512',
'a_GildiaLnBk	': 'https://ofont.ru/view/977',
'a_GrotoSpt	': 'https://ofont.ru/view/970',
'a_JasperTtlRndDrNord	': 'https://ofont.ru/view/1736',
'a_MachinaOrtoSht	': 'https://ofont.ru/view/1321',
'a_MachinaOrtoSpt	': 'https://ofont.ru/view/1566',
'a_ModernoBrk	': 'https://ofont.ru/view/1136',
'a_PlakatCmplRr&Bt	': 'https://ofont.ru/view/3185',
'a_PresentumNrSh	': 'https://ofont.ru/view/2079',
'a_SimplerSpot	': 'https://ofont.ru/view/575',
'AA Lumos	': 'https://ofont.ru/view/4532',
'AbductionCyr	': 'https://ofont.ru/view/2052',
'Abhaya Libre Medium	': 'https://www.fonts-online.ru/font/Abhaya-Libre-Medium',
'Abibas	': 'https://www.fonts-online.ru/font/Abibas',
'Abraxas	': 'https://ofont.ru/view/5284',
'Aero Matics Stencil	': 'https://ofont.ru/view/4535',

'AG_CenturyOldStyle	': 'https://ofont.ru/view/1901',
'Aire Exterior	': 'https://www.fonts-online.ru/font/Aire-Exterior',
'Airfool	': 'https://www.fonts-online.ru/font/Airfool',
'Akademische schmalfette	': 'https://www.fonts-online.ru/font/Akademische-schmalfette',
'Alegreya	': 'https://ofont.ru/view/5239',
'Alegreya SC	': 'https://ofont.ru/view/5250',
'Alice	': 'https://ofont.ru/view/3293',
'Allura	': 'https://www.fonts-online.ru/font/Allura',
'ALOT Gutenberg B	': 'https://ofont.ru/view/52',
'aMavickFont	': 'https://ofont.ru/view/5091',
'Amelia_DG	': 'https://ofont.ru/view/696',
'American Retro	': 'https://ofont.ru/view/3138',
'American TextC	': 'https://ofont.ru/view/3554',
'AmoreCTT	': 'https://ofont.ru/view/4553',
'AngryBirds	': 'https://ofont.ru/view/74',
'Another Danger Slanted(RUS BY L	': 'https://ofont.ru/view/5109',
'Antikvar Shadow	': 'https://ofont.ru/view/3128',
'Aqum	': 'https://ofont.ru/view/5286',
'Archangelsk	': 'https://ofont.ru/view/3022',
'Architectural	': 'https://www.fonts-online.ru/font/Architectural-Regular',
'arial curive	': 'https://ofont.ru/view/4931',
'Arimo	': 'https://www.fonts-online.ru/font/Arimo',
'Art-Decorina	': 'https://ofont.ru/view/2790',
'Artemon	': 'https://ofont.ru/view/4509',
'Arthur Gothic	': 'https://ofont.ru/view/935',
'Arturito Slab	': 'https://ofont.ru/view/5321',
'AsylbekM29.kz	': 'https://ofont.ru/view/106',
'Aurora Script	': 'https://ofont.ru/view/2242',
'Avalon	': 'https://ofont.ru/view/4857',
'Azoft Sans	': 'https://ofont.ru/view/3832',
'Bad Script	': 'https://ofont.ru/view/116',
'Bebas Neue Bold	': 'https://ofont.ru/view/3621',
'Bebas Neue Book	': 'https://ofont.ru/view/3568',
'Bebas Neue Cyrillic	': 'https://ofont.ru/view/4643',
'Bebas Neue Light	': 'https://ofont.ru/view/3478',
'Bebas Neue Regular	': 'https://ofont.ru/view/3379',
'Bebas Neue Thin	': 'https://ofont.ru/view/3692',
'Bebit	': 'https://ofont.ru/view/1773',
'beer money	': 'https://ofont.ru/view/3436',
'BeeskneesCTT	': 'https://ofont.ru/view/1818',
'BetinaScriptC	': 'https://ofont.ru/view/2253',
'Beton Cyr	': 'https://ofont.ru/view/335',
'Bikham Cyr Script	': 'https://ofont.ru/view/2022',
'BIP	': 'https://ofont.ru/view/2625',
'Blacker Sans	': 'https://ofont.ru/view/5265',
'Bodoni Initials	': 'https://ofont.ru/view/2484',
'BraesideLumberboy	': 'https://ofont.ru/view/1786',
'Bravo Cyr	': 'https://ofont.ru/view/2589',
'Bressay Display Trial	': 'https://ofont.ru/view/5254',
'Bressay Trial	': 'https://ofont.ru/view/5248',
'Brozas	': 'https://ofont.ru/view/5287',
'BrushType	': 'https://ofont.ru/view/1931',
'BrushType-SemiBold-Italic	': 'https://ofont.ru/view/765',
'BullionOutlineCyrillic	': 'https://ofont.ru/view/5366',
'Caesar Dressing Cyrillic	': 'https://www.fonts-online.ru/font/CaesarDressingCyrillic',
'Calico Cyrillic	': 'https://ofont.ru/view/3327',
'Capture it	': 'https://ofont.ru/view/4961',
'Capture Smallz	': 'https://www.fonts-online.ru/font/Capture-Smallz',
'Carmen	': 'https://ofont.ru/view/2725',
'Castileo	': 'https://ofont.ru/view/2588',
'CAT Stack	': 'https://www.fonts-online.ru/font/CATStack',
'Caveat	': 'https://ofont.ru/view/5244',

'Chance	': 'https://ofont.ru/view/1019',
'CharakternyC	': 'https://www.fonts-online.ru/font/CharakternyC',
'Chat Noir	': 'https://ofont.ru/view/3068',
'cheeseusaceu	': 'https://ofont.ru/view/4755',
'Cheshirskiy Cat	': 'https://ofont.ru/view/7',
'Chocogirl	': 'https://ofont.ru/view/2229',
'Clickuper	': 'https://www.fonts-online.ru/font/Clickuper',
'Codename Coder Free 4F	': 'https://ofont.ru/view/3356',
'Colatemta	': 'https://www.fonts-online.ru/font/Colatemta',
'CompoShadow	': 'https://ofont.ru/view/2447',
'Cookie	': 'https://www.fonts-online.ru/font/Cookie-Regular',
'Copyist	': 'https://ofont.ru/view/13',
'CoquetteC	': 'https://ofont.ru/view/67',
'Country Western	': 'https://ofont.ru/view/3309',
'Country Western Script	': 'https://ofont.ru/view/3340',
'Country Western Swing	': 'https://ofont.ru/view/4199',
'Country Western Swing Title	': 'https://ofont.ru/view/3302',
'Courier Prime	': 'https://www.fonts-online.ru/font/CourierPrime-Bold',
'Cramaten	': 'https://www.fonts-online.ru/font/Cramaten',
'Crash	': 'https://ofont.ru/view/4132',
'Crosterian	': 'https://www.fonts-online.ru/font/Crosterian',
'Cyntho Next Slab Light	': 'https://ofont.ru/view/5293',
'CyrillicGoth	': 'https://ofont.ru/view/24',

'CyrillicHover	': 'https://ofont.ru/view/2083',
'Danger	': 'https://ofont.ru/view/5108',
'Def Writer | BASE Cyr	': 'https://ofont.ru/view/822',
'Depot Trapharet 2D	': 'https://ofont.ru/view/3233',
'Deutsch Gothic	': 'https://ofont.ru/view/21',
'DexterC	': 'https://ofont.ru/view/218',
'DG_Herold	': 'https://ofont.ru/view/1953',
'Dimbo	': 'https://www.fonts-online.ru/font/Dimbo-Regular',
'Disco 60-s	': 'https://ofont.ru/view/3028',
'DisneyPark	': 'https://ofont.ru/view/30',
'Dobrozrachniy Oblique	': 'https://www.fonts-online.ru/font/Dobrozrachniy-Oblique',
'DollhouseC	': 'https://ofont.ru/view/3118',
'DRAB	': 'https://ofont.ru/view/5322',
'Droid Serif	': 'https://ofont.ru/view/2573',

'DrPoDecorRu	': 'https://ofont.ru/view/54',
'DS Army Cyr	': 'https://ofont.ru/view/1020',
'DS CenturyCapitals	': 'https://ofont.ru/view/1175',
'DS Down Cyr	': 'https://ofont.ru/view/753',
'DS Eraser Cyr	': 'https://ofont.ru/view/357',
'DS Eraser2	': 'https://ofont.ru/view/44',
'DS Goose	': 'https://ofont.ru/view/1446',
'DS Izmir	': 'https://ofont.ru/view/2147',
'DS Kork	': 'https://ofont.ru/view/26',
'DS Note	': 'https://ofont.ru/view/11',
'DS Sharper	': 'https://ofont.ru/view/2609',
'DS SonOf	': 'https://ofont.ru/view/531',
'DS Stain	': 'https://ofont.ru/view/1356',
'DS Stamper	': 'https://ofont.ru/view/2021',
'DS StandartCyr	': 'https://ofont.ru/view/409',
'Dublon	': 'https://ofont.ru/view/4185',
'Duo quadruple	': 'https://ofont.ru/view/5245',
'DXQuant Antiqua Libertin	': 'https://www.fonts-online.ru/font/DXQuant-Antiqua-Libertin',
'Edisson	': 'https://ofont.ru/view/16',
'Edo SZ HQ	': 'https://www.fonts-online.ru/font/EdoSZHQ-Regular',
'Eh_cyr	': 'https://ofont.ru/view/655',
'EIisabethische	': 'https://www.fonts-online.ru/font/EIisabethische',
'Ekaterina Velikaya Two	': 'https://ofont.ru/view/1889',
'elfabe	': 'https://ofont.ru/view/5128',
'Emblema One	': 'https://www.fonts-online.ru/font/Emblema-One',
'Equestria_Cyrillic	': 'https://ofont.ru/view/46',
'Expressway Free	': 'https://ofont.ru/view/2683',
'Facon	': 'https://www.fonts-online.ru/font/Facon-Bold-Italic',
'Fantazyor	': 'https://ofont.ru/view/127',
'FE Wrong	': 'https://ofont.ru/view/5247',
'Federo	': 'https://www.fonts-online.ru/font/Federo',
'Federov2	': 'https://www.fonts-online.ru/font/Federov2-Regular',
'Firenight	': 'https://ofont.ru/view/5246',
'Fita_Poluustav	': 'https://ofont.ru/view/1165',
'FlashRomanBold_DG	': 'https://ofont.ru/view/879',
'Flow Ext	': 'https://www.fonts-online.ru/font/FlowExt',

'Flowerchild	': 'https://ofont.ru/view/2636',
'Fontocide	': 'https://ofont.ru/view/372',
'Foo	': 'https://ofont.ru/view/3424',
'Fortuna Gothic FlorishC	': 'https://ofont.ru/view/2719',
'Franklin Gothic Demi	': 'https://www.fonts-online.ru/font/Franklin-Gothic-Demi',
'FranklinGothBookCTT	': 'https://ofont.ru/view/1377',
'FreeSet	': 'https://ofont.ru/view/4120',
'FreeSet-Bold	': 'https://ofont.ru/view/2017',
'FreeSetBoldTT	': 'https://ofont.ru/view/1915',
'FreeSetTT	': 'https://ofont.ru/view/1834',
'Freestyle	': 'https://ofont.ru/view/1306',
'FreestyleScript	': 'https://ofont.ru/view/370',
'ft19	': 'https://ofont.ru/view/4162',
'Fulbo Argenta	': 'https://www.fonts-online.ru/font/Fulbo-Argenta',
'FuturaEugenia	': 'https://ofont.ru/view/1072',

'FuturaEugeniaCTT	': 'https://ofont.ru/view/337',
'Futurespore Cyrillic	': 'https://www.fonts-online.ru/font/Futurespore-Cyrillic',
'FuturisCTT	': 'https://ofont.ru/view/310',
'Galiver Sans	': 'https://ofont.ru/view/5264',
'Garamond	': 'https://ofont.ru/view/707',
'Garamond_Condenced-Bold	': 'https://ofont.ru/view/737',
'Gardens CM	': 'https://www.fonts-online.ru/font/Gardens-CM-Regular',
'Gasrux	': 'https://www.fonts-online.ru/font/Gasrux',
'Gazeta Titul	': 'https://ofont.ru/view/998',
'Georgia	': 'https://ofont.ru/view/423',
'Glasten	': 'https://ofont.ru/view/831',
'Glober Bold Free	': 'https://ofont.ru/view/3569',
'Golos Text	': 'https://www.fonts-online.ru/font/Golos-Text-Bold',
'Golos Text Black	': 'https://www.fonts-online.ru/font/Golos-Text-Black',
'Golos Text DemiBold	': 'https://www.fonts-online.ru/font/Golos-Text-DemiBold',
'Golos Text Medium	': 'https://www.fonts-online.ru/font/Golos-Text-Medium',
'GoodDog New	': 'https://www.fonts-online.ru/font/GoodDog-New-Regular',
'GothicG	': 'https://ofont.ru/view/10',
'GothicI	': 'https://ofont.ru/view/6',
'GothicRus	': 'https://ofont.ru/view/33',
'GothicRus Condenced	': 'https://ofont.ru/view/34',
'GothicRus2	': 'https://ofont.ru/view/35',
'Gotika Apvalus	': 'https://ofont.ru/view/121',
'Goudy Decor InitialC	': 'https://ofont.ru/view/2618',
'Goudy Decor ShodwnC	': 'https://ofont.ru/view/3182',
'GoudyOld	': 'https://ofont.ru/view/147',
'Graffiti1C	': 'https://ofont.ru/view/86',
'Graffiti2C	': 'https://ofont.ru/view/169',
'Graffiti3CTT	': 'https://ofont.ru/view/2704',
'Grunge	': 'https://ofont.ru/view/709',
'Hailgen Thin	': 'https://ofont.ru/view/5262',
'Handelson Six_CYR	': 'https://ofont.ru/view/5402',
'Harlequinade Madness	': 'https://ofont.ru/view/2735',
'Havana	': 'https://www.fonts-online.ru/font/Havana',
'Hermann-Gotisch NormalC	': 'https://ofont.ru/view/2675',
'Hermann-GotischC	': 'https://ofont.ru/view/93',
'High Sans Serif 7	': 'https://ofont.ru/view/4718',
'HTC Hand	': 'https://ofont.ru/view/81',
'Insula	': 'https://www.fonts-online.ru/font/Insula',
'Intro Cond Light Free	': 'https://ofont.ru/view/3691',
'Intro Script Demo Medium	': 'https://ofont.ru/view/5266',
'InverkrugOutline	': 'https://www.fonts-online.ru/font/Outline-Inverkrug',
'Ireland	': 'https://ofont.ru/view/4098',
'Irmologion kUcs	': 'https://ofont.ru/view/4191',
'Isabella-Decor	': 'https://ofont.ru/view/899',
'Italiano Decor	': 'https://ofont.ru/view/3330',
'Italy A	': 'https://ofont.ru/view/2608',
'ITC Serif Gothic	': 'https://www.fonts-online.ru/font/SerifGothicITCbyBT-Bold',
'Izis Two	': 'https://ofont.ru/view/3215',
'JakobExtraCTT	': 'https://ofont.ru/view/348',
'JetBrains Mono	': 'https://www.fonts-online.ru/font/JetBrains-Mono-Regular',
'Joke	': 'https://ofont.ru/view/1714',
'JournalCTT	': 'https://ofont.ru/view/854',
'Junegull	': 'https://ofont.ru/view/3181',
'K22 Xanthus	': 'https://ofont.ru/view/5113',
'Kanalisirung	': 'https://ofont.ru/view/5204',
'Kankin FREE FONT	': 'https://ofont.ru/view/4016',
'Karmen	': 'https://ofont.ru/view/3261',
'Keetano Gaijin	': 'https://ofont.ru/view/28',
'Keetano Katakana	': 'https://ofont.ru/view/40',
'Keetano Katana KillBill	': 'https://ofont.ru/view/4646',
'Kepler 296	': 'https://ofont.ru/view/5242',
'Kimberley BlCYR	': 'https://ofont.ru/view/5219',
'Kitsch	': 'https://ofont.ru/view/5268',
'Kitsch Text	': 'https://ofont.ru/view/5269',
'klinik	': 'https://ofont.ru/view/4715',
'Konstanting	': 'https://www.fonts-online.ru/font/Konstanting',
'Koryaka Free Rounded	': 'https://ofont.ru/view/5270',
'Kosko Bold	': 'https://ofont.ru/view/5273',
'Kot Leopold	': 'https://ofont.ru/view/42',
'Koysan	': 'https://ofont.ru/view/5240',
'Kudrashov	': 'https://ofont.ru/view/761',
'Kumparsita	': 'https://ofont.ru/view/3206',
'Kuritza	': 'https://ofont.ru/view/1833',
'laCartoonerie(RUS BY LYAJKA)	': 'https://ofont.ru/view/5192',
'Lazurski	': 'https://ofont.ru/view/928',
'LazurskiCTT	': 'https://ofont.ru/view/1024',
'LC Blowzy	': 'https://ofont.ru/view/1054',
'LC Chalk	': 'https://ofont.ru/view/265',
'Le Bourget	': 'https://ofont.ru/view/2749',
'Le Murmure	': 'https://ofont.ru/view/5261',
'Lemon	': 'https://www.fonts-online.ru/font/lemon',
'Lemon Tuesday	': 'https://www.fonts-online.ru/font/Lemon-Tuesday',
'LeoHand	': 'https://ofont.ru/view/5281',
'Lilita One Rus	': 'https://www.fonts-online.ru/font/Lilita-One-Russian',
'Lineage 2 Font	': 'https://ofont.ru/view/37',
'Literaturnaya	': 'https://ofont.ru/view/1134',
'Lkdown	': 'https://www.fonts-online.ru/font/Lkdown',
'Lloyd	': 'https://ofont.ru/view/3014',
'Lobster	': 'https://ofont.ru/view/3622',
'Lombardia	': 'https://ofont.ru/view/2580',
'Lombardina Initial One	': 'https://ofont.ru/view/50',
'Lombardina Initial Two	': 'https://ofont.ru/view/22',
'Lombardina One	': 'https://ofont.ru/view/2689',
'Lombardina Two	': 'https://ofont.ru/view/2750',
'Long Shot Cyrillic	': 'https://www.fonts-online.ru/font/Long-Shot-Cyrillic',
'Mak	': 'https://www.fonts-online.ru/font/Mak-Bold',
'Mak Light	': 'https://www.fonts-online.ru/font/Mak-Light',
'Malahit	': 'https://ofont.ru/view/967',
'Mama	': 'https://www.fonts-online.ru/font/Mama-Regular',
'Marutya	': 'https://ofont.ru/view/5285',
'Mason	': 'https://ofont.ru/view/29',
'Matreshka	': 'https://ofont.ru/view/1088',
'Maver Free	': 'https://ofont.ru/view/5323',
'Metropol 95	': 'https://ofont.ru/view/2656',
'Microsoft Sans Serif	': 'https://ofont.ru/view/133',
'Miratrix Normal	': 'https://ofont.ru/view/5243',
'Mishkin Shrift	': 'https://ofont.ru/view/5174',
'Misirlou Cyr	': 'https://ofont.ru/view/2741',
'Misto	': 'https://ofont.ru/view/5252',
'Moderne Fette SchwabacherC	': 'https://ofont.ru/view/2572',
'Molodo-font	': 'https://ofont.ru/view/5127',
'MonofontoRUS BY Melman	': 'https://www.fonts-online.ru/font/MonofontoRUSBYMelman-Regular',
'mononoki	': 'https://ofont.ru/view/5320',
'Moonchild	': 'https://ofont.ru/view/17',
'Moonlight	': 'https://ofont.ru/view/1497',
'Moulin Rouge	': 'https://ofont.ru/view/3010',
'Moyenage	': 'https://ofont.ru/view/32',
'mr_AkronimG	': 'https://www.fonts-online.ru/font/mr_AkronimG',
'mr_ApexMk3ExtraLightG	': 'https://www.fonts-online.ru/font/ApexMk3-ExtraLight',
'mr_ApexMk3MediumG	': 'https://www.fonts-online.ru/font/ApexMk3-Medium',
'mr_ArenqG	': 'https://www.fonts-online.ru/font/mr_ArenqG',
'mr_Connections_and_Order	': 'https://www.fonts-online.ru/font/Connections_and_Order',
'mr_InsulaG	': 'https://www.fonts-online.ru/font/mr_InsulaG',
'mr_KirucoupageG	': 'https://www.fonts-online.ru/font/mr_KirucoupageG',
'mr_PotraLightG	': 'https://www.fonts-online.ru/font/mr_PotraLightG-Light',
'mr_VlaShu	': 'https://www.fonts-online.ru/font/mr_VlaShu-VlaShu',
'Mutabor	': 'https://ofont.ru/view/5257',
'MyslNarrowC	': 'https://ofont.ru/view/327',
'Natural Mono	': 'https://www.fonts-online.ru/font/Natural-Mono-Regular',
'Natural Mono Alt	': 'https://www.fonts-online.ru/font/Natural-Mono-Alt-Regular',
'Nesovremenny	': 'https://ofont.ru/view/5255',
'NewJournalC	': 'https://ofont.ru/view/532',
'News Cycle	': 'https://www.fonts-online.ru/font/News-Cycle',
'NewStandardOld	': 'https://www.fonts-online.ru/font/New-Standard-Old',
'NewtonC	': 'https://ofont.ru/view/470',
'Nexa Replica RU	': 'https://ofont.ru/view/5259',
'NFS font	': 'https://ofont.ru/view/3167',
'NfS MW	': 'https://ofont.ru/view/69',
'NK57	': 'https://www.fonts-online.ru/font/NK57',
'Noto Sans	': 'https://www.fonts-online.ru/font/Noto-Sans-Italic',

'NTOutline	': 'https://ofont.ru/view/1597',
'Nyasha Sans	': 'https://ofont.ru/view/5271',
'ObitaemOstrov	': 'https://ofont.ru/view/3454',
'Oglavie Unicode	': 'https://www.fonts-online.ru/font/Oglavie-Unicode',
'Oks Free	': 'https://www.fonts-online.ru/font/Oks-Free',
'Old Classic	': 'https://ofont.ru/view/3278',
'Old Comedy	': 'https://ofont.ru/view/1009',
'Old Standard TT	': 'https://www.fonts-online.ru/font/Old-Standard-TT-Italic',
'Olga	': 'https://ofont.ru/view/1103',
'Open Sans	': 'https://ofont.ru/view/3439',

'Ouroboros	': 'https://www.fonts-online.ru/font/Ouroboros-Regular',
'OwnHand	': 'https://ofont.ru/view/4797',
'Pacifico	': 'https://ofont.ru/view/5009',
'PaladinPCRus	': 'https://ofont.ru/view/14',
'PaladinRus	': 'https://ofont.ru/view/4114',
'Palatino Linotype	': 'https://ofont.ru/view/317',
'Palatino-Normal	': 'https://ofont.ru/view/568',
'Panton Rust Heavy	': 'https://ofont.ru/view/5249',
'Panton Rust Script SemiBold	': 'https://ofont.ru/view/5258',
'Parizhel	': 'https://ofont.ru/view/2693',
'Pecita	': 'https://www.fonts-online.ru/font/Pecita',
'Penguin Attack Cyrillic	': 'https://www.fonts-online.ru/font/PenguinAttackCyrillic',
'PentaGram?s Malefissent	': 'https://ofont.ru/view/4743',
'PerfectDOSVGA437	': 'https://www.fonts-online.ru/font/PerfectDOSVGA437',
'Petersburg	': 'https://ofont.ru/view/692',
'PF DaVinci Script Pro Inked	': 'https://ofont.ru/view/3230',
'PF Hellenica Serif Pro	': 'https://ofont.ru/view/4568',
'PF Playskool Pro	': 'https://ofont.ru/view/3154',
'PF Stamps Pro	': 'https://ofont.ru/view/4567',
'PF Stamps Pro Blur	': 'https://ofont.ru/view/3308',
'PF Stamps Pro Flex	': 'https://ofont.ru/view/4617',
'Plain	': 'https://ofont.ru/view/1687',
'Play	': 'https://www.fonts-online.ru/font/Play-Bold',
'Play Rus	': 'https://ofont.ru/view/1239',

'Pollock2C	': 'https://ofont.ru/view/4074',
'Pollock3C	': 'https://ofont.ru/view/3315',
'Pollock4C	': 'https://ofont.ru/view/4075',
'Pollock4CTT	': 'https://ofont.ru/view/2908',
'Pomidorko	': 'https://www.fonts-online.ru/font/Pomidorko',
'Porsche	': 'https://ofont.ru/view/1383',
'Pragmatica	': 'https://ofont.ru/view/239',
'Prata	': 'https://ofont.ru/view/5280',
'Pribambas	': 'https://www.fonts-online.ru/font/Pribambas-Regular',
'Proletariat	': 'https://ofont.ru/view/2765',
'Pudelina	': 'https://ofont.ru/view/3108',
'Pudelinka	': 'https://ofont.ru/view/27',
'Quake Cyr	': 'https://ofont.ru/view/135',
'Quant Antiqua	': 'https://ofont.ru/view/991',
'Rahovets	': 'https://ofont.ru/view/5256',
'Raleway	': 'https://ofont.ru/view/3448',

'Raskalnikov	': 'https://ofont.ru/view/4593',
'Ratanegra Cyrillic	': 'https://ofont.ru/view/4540',
'RAZMAHONT	': 'https://ofont.ru/view/128',
'Red October Stencil	': 'https://ofont.ru/view/97',
'Regular	': 'https://ofont.ru/view/5277',
'Renaldo Modern	': 'https://ofont.ru/view/3012',
'RetroVille NC	': 'https://www.fonts-online.ru/font/RetroVille-NC',
'Ritalin	': 'https://www.fonts-online.ru/font/Ritalin',

'Ritalin ExtraBold	': 'https://www.fonts-online.ru/font/Ritalin-ExtraBold',
'Roboto	': 'https://ofont.ru/view/3062',

'Roboto Black	': 'https://ofont.ru/view/3296',
'Roboto Thin	': 'https://ofont.ru/view/3292',
'Robotron Dot Matrix	': 'https://www.fonts-online.ru/font/Robotron-Dot-Matrix',
'Rockletter Simple	': 'https://ofont.ru/view/98',
'Romashulka	': 'https://ofont.ru/view/59',
'Romvel	': 'https://ofont.ru/view/4630',
'Rondo Ancient Two	': 'https://ofont.ru/view/2536',
'Rose Versailles1	': 'https://ofont.ru/view/3354',
'Rostov	': 'https://www.fonts-online.ru/font/Rostov',
'Rounded Sans Serif 7	': 'https://ofont.ru/view/4889',
'Rounds Black	': 'https://ofont.ru/view/3645',
'Rublik	': 'https://ofont.ru/view/1313',
'RuinedC	': 'https://ofont.ru/view/1408',
'RUSBoycott	': 'https://ofont.ru/view/62',
'Ruslan Display	': 'https://ofont.ru/view/114',
'Samba	': 'https://ofont.ru/view/84',
'Schwabacher	': 'https://ofont.ru/view/4956',
'Screpka	': 'https://ofont.ru/view/5260',
'Segoe Print	': 'https://ofont.ru/view/2089',
'Serifiqo 4F Free Capitals	': 'https://ofont.ru/view/3776',
'Single Day	': 'https://www.fonts-online.ru/font/Single-Day-Regular',
'Sinkin Sans 600 SemiBold Italic	': 'https://www.fonts-online.ru/font/Sinkin-Sans-600-SemiBold-Italic',
'SkolSerifa	': 'https://www.fonts-online.ru/font/SkolSerifa',
'SlimamifMedium	': 'https://www.fonts-online.ru/font/SlimamifMedium',
'SMW Text 2 NC	': 'https://www.fonts-online.ru/font/SMW-Text-2-NC',
'SMW Text NC	': 'https://www.fonts-online.ru/font/SMW-Text-NC',
'Snowflake	': 'https://ofont.ru/view/5357',
'SonyEricssonLogo	': 'https://ofont.ru/view/4722',
'Source Sans Pro	': 'https://www.fonts-online.ru/font/Source-Sans-Pro',
'Source Serif Pro	': 'https://ofont.ru/view/5241',
'Source Serif Pro Light	': 'https://ofont.ru/view/5251',
'Sowjetschablone	': 'https://www.fonts-online.ru/font/Sowjetschablone',
'Special Elite	': 'https://www.fonts-online.ru/font/Special-Elite',
'Sports World	': 'https://www.fonts-online.ru/font/SportsWorld',
'StandardPosterCTT	': 'https://ofont.ru/view/483',
'Standout	': 'https://ofont.ru/view/5146',
'STARGATE	': 'https://ofont.ru/view/123',
'Start	': 'https://ofont.ru/view/510',
'STIX Two Math	': 'https://www.fonts-online.ru/font/STIX-Two-Math',
'STIX Two Text	': 'https://www.fonts-online.ru/font/STIX-Two-Text',
'StudioScriptCTT	': 'https://ofont.ru/view/1067',
'Super Mario 3D World	': 'https://ofont.ru/view/5211',
'Swampy	': 'https://ofont.ru/view/5253',
'TagirCTT	': 'https://ofont.ru/view/922',
'Taraxacum	': 'https://ofont.ru/view/5288',
'TenseC	': 'https://ofont.ru/view/212',
'Terminator Cyr	': 'https://ofont.ru/view/2448',
'Theano Didot	': 'https://ofont.ru/view/3643',
'Thintel	': 'https://www.fonts-online.ru/font/Thintel',
'TimelessTCYLig	': 'https://ofont.ru/view/661',
'Times New Roman	': 'https://ofont.ru/view/158',
'TimesET 90	': 'https://ofont.ru/view/557',
'TimesET95N	': 'https://ofont.ru/view/1441',
'To Japan	': 'https://ofont.ru/view/5342',
'Tolstyak	': 'https://ofont.ru/view/4282',
'Toscania	': 'https://ofont.ru/view/109',
'Trafaret	': 'https://ofont.ru/view/1458',
'Trafaret Kit	': 'https://ofont.ru/view/4804',
'Trafaret Kit Hatched	': 'https://ofont.ru/view/4752',
'Trajan Pro 3	': 'https://ofont.ru/view/4971',
'Traktir-Modern	': 'https://ofont.ru/view/173',
'Trappist	': 'https://ofont.ru/view/5267',
'TriodPostnaja	': 'https://ofont.ru/view/3381',
'Troika	': 'https://ofont.ru/view/3631',
'Truetypewriter PolyglOTT	': 'https://www.fonts-online.ru/font/Truetypewriter-PolyglOTT',
'TT2020Base	': 'https://www.fonts-online.ru/font/TT2020Base',
'Typesauce	': 'https://www.fonts-online.ru/font/Typesauce',
'TypeWriter	': 'https://ofont.ru/view/670',
'Ubuntu	': 'https://ofont.ru/view/2525',
    'Uk_Antique	': 'https://ofont.ru/view/4368',
'UkrainianFreeSet	': 'https://ofont.ru/view/2540',
    'Unicephalon Cyrillic	': 'https://www.fonts-online.ru/font/UnicephalonCyrillic',
'Universe	': 'https://www.fonts-online.ru/font/Universe',
'UntitledJapanese	': 'https://ofont.ru/view/5308',
'Ustroke	': 'https://www.fonts-online.ru/font/Ustroke-Regular',
'UsualNew	': 'https://ofont.ru/view/275',
    'Veles	': 'https://ofont.ru/view/4012',
'Venus Rising Cyrillic	': 'https://ofont.ru/view/4440',
'Verona Gothic	': 'https://ofont.ru/view/958',
'Verona Gothic Flourishe	': 'https://ofont.ru/view/498',
'Victorian Gothic One	': 'https://ofont.ru/view/75',
'Vinque Rg	': 'https://ofont.ru/view/5324',
    'void	': 'https://www.fonts-online.ru/font/void-thin',
    'Vollkorn	': 'https://ofont.ru/view/3805',
    'Wetware Cyrillic	': 'https://www.fonts-online.ru/font/Wetware-Cyrillic',
'XAYAX	': 'https://ofont.ru/view/5232',
'Zipper1 Cyr	': 'https://ofont.ru/view/2163',
'Znikomit	': 'https://ofont.ru/view/4055',
'Zrnic Cyr	': 'https://ofont.ru/view/1428',
'Zschusch	': 'https://www.fonts-online.ru/font/Zschusch',
'Zyablik	': 'https://ofont.ru/view/5384',
}

